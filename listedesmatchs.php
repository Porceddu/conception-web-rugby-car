<?php session_start(); 
// connexion à MySQL 
include("includes/param.inc.php"); 
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Rugby-CAR</title>
		<link rel="stylesheet" href="style.css">
	</head>
	<body>
		<?php include('header.acc.php'); ?>
		<?php if(isset($_SESSION['email'])) {
			include('aside.connecte.php');
		} else { include('aside.acc.php'); }?> 


<section id="liste">

<h1>Liste des matchs</h1>

<table class="table table-bordered table-hover table-striped">
   
    <thead>
	
        <tr>
            <th>Equipe 1</th>
            <th>Equipe 2</th>
            <th>Stade</th>
            <th>Date</th>
            
        </tr>
    </thead>
	
	<?php
$mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
if ($mysqli->connect_errno) {
	echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno. ") " . $mysqli->connect_error;
} else {
	$res=$mysqli->query("SELECT * FROM matches WHERE dateMatch >= CURDATE()");
	if(!$res->num_rows){
		echo "<p>aucun resultat</p>";
	} else { 
		while($tuple=$res->fetch_assoc()){
			echo '<tr><td><p>'.htmlentities($tuple['team1match']).'</p></td>'; 
			echo '<td><p>'.htmlentities($tuple['team2match']).'</p></td>'; 
			echo '<td><p>'.htmlentities($tuple['stadeMatch']).'</p></td>'; 
			echo '<td><p>'.htmlentities($tuple['dateMatch']).'</p></td></tr>';  
		}
	}
 }
 ?>
</table>
</section>
</body>
</html>
