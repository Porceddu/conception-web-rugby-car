-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Dim 08 Novembre 2015 à 20:46
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `crunchcovoit`
--

-- --------------------------------------------------------

--
-- Structure de la table `matches`
--

CREATE TABLE IF NOT EXISTS `matches` (
  `idMatch` int(255) NOT NULL,
  `team1match` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `team2match` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `dateMatch` datetime NOT NULL,
  `stadeMatch` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idMatch`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `matches`
--

INSERT INTO `matches` (`idMatch`, `team1match`, `team2match`, `dateMatch`, `stadeMatch`) VALUES
(1, 'Angleterre', 'fidji', '2015-11-15 21:00:00', 'Londres'),
(2, 'Tonga', 'Georgie', '2015-11-16 13:00:00', 'Gloucester'),
(3, 'Irlande', 'Canada', '2015-11-17 15:00:00', 'Cardiff'),
(4, 'Afrique du sud', 'Japon', '2015-11-18 17:00:00', 'Brighton'),
(5, 'France', 'Italie', '2015-11-19 21:00:00', 'Londres'),
(6, 'Samoa', 'USA', '2015-11-20 15:00:00', 'Gloucester');

-- --------------------------------------------------------

--
-- Structure de la table `membre`
--

CREATE TABLE IF NOT EXISTS `membre` (
  `idmembre` int(255) NOT NULL AUTO_INCREMENT,
  `prenom` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `nom` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `phone` int(10) NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` char(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idmembre`),
  UNIQUE KEY `email` (`email`),
  KEY `idmembre` (`idmembre`),
  FULLTEXT KEY `prenom` (`prenom`,`nom`,`password`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Contenu de la table `membre`
--

INSERT INTO `membre` (`idmembre`, `prenom`, `nom`, `phone`, `email`, `password`) VALUES
(8, 'Lorelei', 'Petitcuenot', 778206210, 'petitcuenot.l@hotmail.fr', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'),
(9, 'Vincent', 'Tourkia', 606060606, 'vincent@gmail.com', 'dd7b6bd34d350534db81c3322c7ac4e7ef0b0439aba8e44f15a20e231f234fc8'),
(14, 'val', 'valll', 666644820, 'val@gmail.com', '891e12e156d8c6609c6d5f3e04b2fc8da6d9ff3d7e9f906314c0909da69637eb'),
(15, 'abdel', 'azdouz', 60606006, 'azdouz@gmail.com', 'ed02457b5c41d964dbd2f2a609d63fe1bb7528dbe55e1abf5b52c249cd735797'),
(17, 'momo', 'm', 666644820, 'mo@gmail.com', 'bcb15f821479b4d5772bd0ca866c00ad5f926e3580720659cc80d39c9d09802a');

-- --------------------------------------------------------

--
-- Structure de la table `trajet`
--

CREATE TABLE IF NOT EXISTS `trajet` (
  `idTrajet` int(255) NOT NULL AUTO_INCREMENT,
  `nbPlaceTrajet` int(30) NOT NULL,
  `prixTrajet` int(255) NOT NULL,
  `villeDepartTrajet` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `idMatchTrajet` int(255) NOT NULL,
  `nombreResa` int(100) NOT NULL DEFAULT '0',
  `idMembrecond` int(100) NOT NULL,
  `date` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idTrajet`),
  KEY `idMatchTrajet` (`idMatchTrajet`),
  KEY `idMembre` (`idMembrecond`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

-- --------------------------------------------------------

--
-- Structure de la table `voyage`
--

CREATE TABLE IF NOT EXISTS `voyage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idVoyage` int(255) NOT NULL,
  `idMembre` int(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idVoyage` (`idVoyage`,`idMembre`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
