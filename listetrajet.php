<?php session_start(); 
include_once("./includes/param.inc.php"); 

$mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Rugby-CAR</title>
		<link rel="stylesheet" href="style.css">
		<script src="sweetalert-master/dist/sweetalert.min.js"></script>
		<link rel="stylesheet" type="text/css" href="sweetalert-master/dist/sweetalert.css">
	</head>
	<body>
		<?php include('header.acc.php'); ?>
		<?php if(isset($_SESSION['email'])) {
			include('aside.connecte.php');
		} else { include('aside.acc.php'); }?>

		<?php
		$res=$mysqli->query("SELECT * FROM trajet INNER JOIN matches ON idMatch = idMatchTrajet WHERE idTrajet = ".$_SESSION['id']."");
		$tuple=$res->fetch_assoc();
		if(isset($_GET['action']) and isset($_GET['id'])){
				$lidtrajet = $_GET['id'];
				mysqli_query($mysqli, 'insert into voyage (idVoyage, idMembre) values ("'.$_GET['id'].'", "'.$_SESSION['id'].'")');
				mysqli_query($mysqli, "UPDATE trajet SET nombreResa = nombreResa + 1 WHERE $lidtrajet = idTrajet");
				$_SESSION['message'] = "Vous avez bien rejoint le trajet !";
				echo '<script>swal("Félicitation votre réservation à bien été ajoutée !", "Vous allez être redirigé vers la liste des trajets", "success")</script>';
				$time = 2;
	            $url = "listetrajet.php";
	            echo '<meta http-equiv="refresh" content="',$time,';url=',$url,'">';
				//exit();
		}
		?>

		<section id="liste">
			<?php
		if(isset($_SESSION['message'])){
			echo $_SESSION['message'];
			unset( $_SESSION['message']);
		}
		?>
		<h1>Liste des trajets</h1>

		<table class="table table-bordered table-hover table-striped">
		   
		    <thead>
			
		        <tr>
		            <th>Match</th>
		            <th>Nombre de places</th>
		            <th>Ville de départ</th>
		            <th>Prix</th>
		            <th>Date</th>
		            <th>Trajets</th>
		        </tr>
		    </thead>
			
			<?php    
		$mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
		if ($mysqli->connect_errno) {
			echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno. ") " . $mysqli->connect_error;
		} else {
			$tot = $mysqli->query("SELECT * FROM voyage INNER JOIN trajet ON idTrajet = idVoyage") ; 
			$resu = $mysqli->query("SELECT * FROM matches INNER JOIN trajet ON idMatch = idMatchTrajet WHERE trajet.date >= CURDATE()");
			//$rs = mysqli_fetch_array($resu, MYSQLI_ASSOC);
			$res=$mysqli->query("SELECT * FROM trajet INNER JOIN matches ON idMatch = idMatchTrajet WHERE trajet.date >= CURDATE()");
			if(!$res->num_rows){
				echo "<p>aucun resultat</p>";
			} else { 
				while($tuple=$res->fetch_assoc() and $tuple2=$resu->fetch_assoc() ){
					echo '<tr><td><p>'.htmlentities($tuple2['team1match']).' / '.htmlentities($tuple2['team2match']).'</p></td>'; 
					echo '<td><p>'.htmlentities($tuple['nbPlaceTrajet']).'</p></td>'; 
					echo '<td><p>'.htmlentities($tuple['villeDepartTrajet']).'</p></td>'; 
					echo '<td><p>'.htmlentities($tuple['prixTrajet']).'&euro;</p></td>';
					echo '<td><p>'.htmlentities($tuple['date']).'</p></td>';
					if($tuple['nombreResa'] >= $tuple['nbPlaceTrajet']){
						echo "<td><p> Complet </p></td></tr>";
					}elseif($tuple['idMembrecond'] == $_SESSION['id']){
						echo "<td><p> C'est votre trajet !</p></td></tr>";
					}else{$restant = $tuple2['nbPlaceTrajet'] - $tuple2['nombreResa'];
					?><td><p><input type="submit" name="lienrejoindretrajet" value="rejoindre trajet" onclick="self.location.href='listetrajet.php?action=rejoindre&amp;id=<?php echo $tuple['idTrajet']; ?>'" > <?php  echo" ( ".$restant." place(s) restante(s) ) "; ?> </p></td></tr><?php 
				}}
			}
		 }
		 ?>
		</table>
		<br><br>
		<input type="button" name="lienajoutertrajet" value="ajouter trajet" onclick="self.location.href='ajoutertrajet.php'" id="bouttonajout">
		<br><br>
		</section>

	</body>
</html>