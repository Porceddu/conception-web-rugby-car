// Instanciation de la classe Autocompleter, pour le champ de saisie "villes"
new Ajax.Autocompleter(
    "villes",   // id du champ de formulaire
    "villes_propositions",  // id de l'élément utilisé pour les propositions
    "ajoutertrajet.php",  // URL du script côté serveur
    {
        paramName: 'ville',  // Nom du paramètre reçu par le script serveur
        minChars: 2,   // Nombre de caractères minimum avant que des appels serveur ne soient effectués
        tokens: [',', ';'],
        indicator: 'indicateur-chargement-ville',
        afterUpdateElement : function (input, li) {
                // Fonction appelée après choix de l'utilisateur
                $('code_insee').innerHTML = 'Code INSEE choisi : ' + li.id;
                $('code_insee').show();
            }
    });