<?php @session_start(); ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Rugby-CAR</title>
		<link rel="stylesheet" href="style.css">
	</head>
	<body >
		<?php include('header.acc.php'); ?>
		<?php if(isset($_SESSION['email'])) {
			include('aside.connecte.php');
		} else { include('aside.acc.php'); }?> 
		<section id="accueilmoi">
			<h1 id="titreacc">Site de covoiturage pour matchs de Rugby</h1>
				<p >Pour la Coupe du Monde de Rugby organisée par nos amis Anglais. <br><br>

				Soucieuse de la préservation de l’environnement autant que de la présence massive de supporters Français dans les stades, la Fédération Française de Rugby met à votre disposition cette plate-forme de covoiturage vous permettant de proposer un trajet vers un stade donné pour un match donné ou de rejoindre un trajet existant.<br><br><br>
				
				Vous pouvez accéder à la liste des matchs en tant que visiteur.<br>
				Si vous voulez bénéficier de tous les services proposés par notre site... <a href="inscription.php">Inscrivez-vous !</a> 
				</p>
		</section>	
	</body>
</html>