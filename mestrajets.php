<?php session_start(); 
include_once("includes\param.inc.php"); 
$mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Rugby-CAR</title>
		<link rel="stylesheet" href="style.css">
	</head>
	<body>
		<?php include('header.acc.php'); ?>
		<?php if(isset($_SESSION['email'])) {
			include('aside.connecte.php');
		} else { include('aside.acc.php'); }?> 
	<section>
		
		<h1>Mes trajets proposés</h1>
			<p>
				<?php 		
				$res = $mysqli->query("SELECT * FROM trajet INNER JOIN  matches ON idMatch = idMatchTrajet WHERE idmembrecond = ".$_SESSION['id']."");
				$resu = $mysqli->query("SELECT * FROM matches INNER JOIN trajet ON idMatch = idMatchTrajet WHERE idMatch = idMatchTrajet");
				
				if(!$res->num_rows){
					echo "Vous n'avez ajouté aucuns trajets, mais vous pouvez <a href=\"ajoutertrajet.php\">ajouter vos trajets</a>";
				} else { 
					  
					while($tuple=$res->fetch_assoc() ){
						echo '<br> <br>Info du trajet : <br>';
						echo 'Match : '.htmlentities($tuple['team1match']).' / '.htmlentities($tuple['team2match']);
						echo ' départ de : '.htmlentities($tuple['villeDepartTrajet']);
						echo ' le ' . substr(htmlentities($tuple['date']), 0, 10) . ' à ' . substr(htmlentities($tuple['date']), 11).', ';
						echo htmlentities($tuple['nbPlaceTrajet']).' places proposées à ';
						echo htmlentities($tuple['prixTrajet']).'&euro; ';
						
						$resul = $mysqli->query("SELECT * FROM voyage INNER JOIN membre ON membre.idmembre = voyage.idMembre WHERE voyage.idVoyage = '".$tuple['idTrajet']."' ");
						if($resul->num_rows){
							echo'<br>Voyageurs :<br>';
							while($tuple3=$resul->fetch_assoc()){
							
							echo "".htmlentities($tuple3['prenom'] )." ";
							echo " ".htmlentities($tuple3['nom'])."<br>";
							};
						}else{
							echo'<br>Voyageurs :<br>';
							echo '<br>Il n\'y a aucunes réservations';
							
						}
					};
					
				}

				?>
			</p>






		<h1>Mes réservations</h1>
			<p>
				<?php 	
					$result = $mysqli->query("SELECT * FROM voyage INNER JOIN trajet ON idVoyage = idTrajet INNER JOIN matches ON trajet.idMatchTrajet = matches.idMatch WHERE voyage.idMembre = ".$_SESSION['id']."");
					if(!$result->num_rows){
						echo "Vous n'avez pas de réservations effectuées, mais vous pouvez <a href=\"listetrajet.php\">réserver</a>";
					} else { 
						  
						while($tuple=$result->fetch_assoc() ){
							echo '<br> <br>Info du trajet : <br>';
							echo 'Match : '.htmlentities($tuple['team1match']).' / '.htmlentities($tuple['team2match']);
							echo ' départ de : '.htmlentities($tuple['villeDepartTrajet']);
							echo ' le ' . substr(htmlentities($tuple['date']), 0, 10) . ' à ' . substr(htmlentities($tuple['date']), 11).', ';
							echo htmlentities($tuple['nbPlaceTrajet']).' places proposées à ';
							echo htmlentities($tuple['prixTrajet']).'&euro; ';
							
							
						};
						
					}

				?>
			</p>
	</section>

		
	</body>
</html>






