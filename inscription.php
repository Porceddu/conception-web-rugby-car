<?php session_start();
//var_dump($_POST);
// connexion à MySQL 
include_once("./includes/param.inc.php"); 

$mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);



/* Vérification de la connexion 
if ($mysqli->connect_error) {
    echo 'connexion impossible... :'.$mysqli->connect_error;
}
else {
    echo 'connexion réussie : '.$mysqli->host_info;
}
*/ ?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Rugby-CAR</title>
		<link rel="stylesheet" href="style.css">
	</head>
	<body>
        <?php include('header.acc.php'); ?>
        <?php if(isset($_SESSION['email'])) {
            include('aside.connecte.php');
        } else { include('aside.acc.php'); }?> 
		<!--<?php //include('header.acc.php'); ?>
		<?php //if(isset($_SESSION)){
			//include('aside.acc.php');
		//}else {include('aside.connecte.php'); }?>--> 

<?php

//On verifie que le formulaire a ete envoye
if(isset($_POST['prenom'], $_POST['nom'], $_POST['email'], $_POST['telephone'], $_POST['password'], $_POST['passverif']) and $_POST['email']!='')
{
        //On enleve lechappement si get_magic_quotes_gpc est active
        if(get_magic_quotes_gpc())
        {
                $_POST['prenom'] = stripslashes($_POST['prenom']);
                $_POST['nom'] = stripslashes($_POST['nom']);
                $_POST['email'] = stripslashes($_POST['email']);
                $_POST['telephone'] = stripslashes($_POST['telephone']);
                $_POST['password'] = stripslashes($_POST['password']);
                $_POST['passverif'] = stripslashes($_POST['passverif']);
               }
        //On verifie si le mot de passe et celui de la verification sont identiques
        if($_POST['password']==$_POST['passverif'])
        {
                //On verifie si le mot de passe a 6 caracteres ou plus
                if(strlen($_POST['password'])>=6)
                {
                        //On verifie si lemail est valide
                        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false) {
                            //On vérifie si le uméro de télephone est valide 
                            if(preg_match("#^0[1-68]([-. ]?[0-9]{2}){4}$#", $_POST['telephone'])){
                                //On echape les variables pour pouvoir les mettre dans une requette SQL
                                $prenom = mysqli_real_escape_string($mysqli, $_POST['prenom']);
                                $nom = mysqli_real_escape_string($mysqli, $_POST['nom']);
                                $email = mysqli_real_escape_string($mysqli, $_POST['email']);
                                //$password = mysqli_real_escape_string($mysqli, $_POST['password']);
                                $passverif = mysqli_real_escape_string($mysqli, $_POST['passverif']);
                                $telephone = mysqli_real_escape_string($mysqli, $_POST['telephone']);
                                $password = hash("sha256", $_POST['password']);
                                //On verifie sil ny a pas deja un utilisateur inscrit avec le pseudo choisis
                                $dn = mysqli_num_rows(mysqli_query($mysqli, 'select idmembre from membre where email="'.$email.'"'));
                                if($dn==0)
                                {
                                        //On enregistre les informations dans la base de donnee
                                        if(mysqli_query($mysqli, 'insert into membre (nom, prenom, email, phone, password) values ("'.$nom.'", "'.$prenom.'", "'.$email.'", "'.$telephone.'", "'.$password.'")'))
                                        {
                                                //Si ca a fonctionne, on naffiche pas le formulaire
                                                $form = false;
                                                $login = mysqli_escape_string($mysqli, $_POST['email']);
                                                $_SESSION['email'] = $login;
                                                $_SESSION['prenom'] = $prenom;
                                                $_SESSION['nom'] = $nom;
                                                $_SESSION['id'] = $mysqli->insert_id;
                                                echo '<meta http-equiv="refresh" content="0; URL=accueil.php">';
                                        }
                                        else
                                        {
                                                //Sinon on dit quil y a eu une erreur
                                                $form = true;
                                                $message = 'Une erreur est survenue lors de l\'inscription.';
                                        }
                                }else{
                                    //Sinon, on dit que le pseudo voulu est deja pris
                                    $form = true;
                                    $message = 'Un autre utilisateur utilise deja l\'email que vous desirez utiliser.';
                                }
                            }else{
                                //Sinon, on dit que le numéro n'est pas valide
                                $form = true;
                                $message = 'le numéro de télephone n\'est pas valide ! (0606060606)';
                            }
                        }else{
                            //Sinon, on dit que l'email nest pas valide
                            $form = true;
                            $message = 'L\'email que vous avez entr&eacute; n\'est pas valide.';
                        }
                }else{
                    //Sinon, on dit que le mot de passe nest pas assez long
                    $form = true;
                    $message = 'Le mot de passe que vous avez entree; contient moins de 6 caracteres.';
                }
        }else{
            //Sinon, on dit que les mots de passes ne sont pas identiques
            $form = true;
            $message = 'Les mots de passe que vous avez entr&eacute; ne sont pas identiques.';    
        }
}else{
    $form = true;

       
}
if($form)
{
        
       ?>

       <section >
        <?php
        //On affiche un message s'il y a lieu
        if(isset($message))
        {
              ?><div class="message"> <?php echo $message ;?>  </div><?php
        }
        ?>
		<h1>Inscription</h1>
        <div id="inscription">
		<form method="post" action="inscription.php">
			
				<label class="ins" for="prenom">Prenom</label>
				<input class="ins" type="text" id="prenom" name="prenom" placeholder="prenom" required><br>
				<label class="ins" for="mail">Nom</label>
				<input class="ins" type="text" id="nom" name="nom" placeholder="nom" required><br>
				<label class="ins" for="telephone">Numéro de télephone</label>
				<input class="ins" type="text" id="telephone" name="telephone" placeholder="telephone" required><br>
				<label class="ins" for="mail">Email</label>
				<input class="ins" type="text" id="mail" name="email" placeholder="xxx@yyy.zzz" required><br>
				<label class="ins" for="password">Mot de passe</label>
				<input class="ins" type="password" id="password" name="password" placeholder="*******" required><br>
				<label class="ins" for="verifpassword">Vérification mot de passe</label>
				<input class="ins" type="password" id="verifpassword" name="passverif" placeholder="*******" required><br>
				<button class="ins" type="submit" name="Iscription" value="ok">Inscription</button>
				<button class="ins" type="button" name="Annuler" onclick="self.location.href='accueil.php'" value="ok">Annuler</button>
			
		</form>
		</div>
  
  
	</section>

       <?php
}

?>

	</body>
</html>