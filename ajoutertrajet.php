<?php session_start(); 
include_once("./includes/param.inc.php"); 

$mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Rugby-CAR</title>
		<link rel="stylesheet" href="style.css">
		<script src="sweetalert-master/dist/sweetalert.min.js"></script>
		<link rel="stylesheet" type="text/css" href="sweetalert-master/dist/sweetalert.css">
		<link rel="stylesheet" type="text/css" href="datetimepicker-master/jquery.datetimepicker.css">
				
	</head>
	<body>
		<?php include('header.acc.php'); ?>
		<?php if(isset($_SESSION['email'])) {
			include('aside.connecte.php');
		} else { include('aside.acc.php'); }?>

<?php

//Vérification que le formulaire à été envoyé
if(isset($_POST['Match'],$_POST['date'], $_POST['Prix'], $_POST['Nbre'], $_POST['ville']) and ($_POST["Prix"] !="") and ($_POST["Nbre"] !="") and ($_POST["ville"] !=""))
{	
	if(!($_POST["Match"] == 0)){
			//Supprime les antislashs d'une chaîne
		if(get_magic_quotes_gpc())
			{
				$_POST['Match'] = stripcslashes($_POST['Match']);
				$_POST['date'] = stripcslashes($_POST['Match']);
				$_POST['Nbre'] = stripcslashes($_POST['Nbre']);
				$_POST['Prix'] = stripcslashes($_POST['Prix']);
				$_POST['ville'] = stripcslashes($_POST['ville']);
			}
		//if($_POST['Nbre']>)
			if(filter_var($_POST['Nbre'], FILTER_VALIDATE_INT) !== false and strtotime($_POST['date']) != 0 and strtotime($_POST['date']) > time()){
				
				$resuu=$mysqli->query("SELECT dateMatch FROM matches WHERE idMatch = ".$_POST['Match']."");
				$row=mysqli_fetch_assoc($resuu);
				$id = $row['dateMatch'];
				if(strtotime($_POST['date']) <= strtotime($id)){
					$match = mysqli_real_escape_string($mysqli, $_POST['Match']);
					$date = strtotime($_POST['date']);
			        $ville = mysqli_real_escape_string($mysqli, $_POST['ville']);
			        $prix = mysqli_real_escape_string($mysqli, $_POST['Prix']);
			        $Nbre = mysqli_real_escape_string($mysqli, $_POST['Nbre']);
			        //On enregistre les informations dans la base de donnee
				    if(mysqli_query($mysqli, 'insert into trajet (nbPlaceTrajet, prixTrajet, villeDepartTrajet, idMatchTrajet, idMembrecond,date) values ("'.$Nbre.'", "'.$prix.'", "'.$ville.'", "'.$match.'", "'.$_SESSION['id'].'", FROM_UNIXTIME("'.$date.'"))')
				    	)
				    {
				            //Si ca a fonctionne, on naffiche pas le formulaire
				            $form = false;
				            echo '<script>swal("Félicitation votre trajet à bien été ajouté !", "Vous allez être redirigé vers la liste des trajets", "success")</script>';
				            $time = 5;
				            $url = "listetrajet.php";
				            echo '<meta http-equiv="refresh" content="',$time,';url=',$url,'">';
				    }
				    else
				    {
				            //Sinon on dit quil y a eu une erreur
				            $form = true;
				            $message = 'Une erreur est survenue lors de l\'ajout du trajet';
				    }
				}else{
					$form = true;
		    		$message = 'Attention la date saisie est supérieure à celle du match !  ( '.($id).' )';
				}
			}else{
				$form = true;
			    $message = 'Veuillez vérifier les informations de la date saisies !';
			}
		}else{
		$form = true;
		$message = 'Veuillez renseigner tous les champs !';
}
        
	//$match = $_POST["Match"];
	//$prix = $_POST["Prix"];
	//$ville = $_POST['ville'];
	//echo "<span>valeur selectionnée : $match</span>";
	
}

?>

	<section id="ajoutertrajett">
		<?php
		if(isset($message))
        {
              ?><div class="message"> <?php echo $message ;?>  </div><?php
        }
		?>
		<h1 id="ajouterTrajet">Ajouter trajet</h1>
		<div id="ajouttrajet">
		<form method="post" action="ajoutertrajet.php" >
			<div id="aaligner">
				<?php 
				$recherche = "SELECT * FROM matches ORDER BY team1match" ; 
				$query = mysqli_query($mysqli, $recherche) ?>

			<select name="Match" id="drop" >
				<option value="">Match</option>
				<?php while ($rs = mysqli_fetch_array($query, MYSQLI_ASSOC)) { ?>
				<option value="<?php echo $rs["idMatch"]; ?>"><?php echo $rs["team1match"],' / ',  $rs["team2match"] ; ?></option>
				
				<?php } ?>
			</select> <br>		
				<label class="ins" for="datetimepicker">Date et heure de départ</label>
				<input class="ins" id="datetimepicker" type="text" name="date" ><br>			
				<label class="ins" for="Nbredeplace">Nbre de place</label>
				<input class="ins" type="number" id="Nbredeplace" min="1" name="Nbre" placeholder="Nbre de place" required><br>
				<label class="ins" for="Prix">Prix</label>
				<input class="ins" type="number" id="Prix" min="1" name="Prix" placeholder="Prix" required>  &euro;<br>
				<label class="ins" for="villedepart">ville départ</label>
				<input class="ins" type="text" id="villedepart" name="ville" placeholder="ville départ" required><br>
				<input id="ajoutertr" type="submit" value="Ajouter"> 
			</div>
		</form>
	</div>
	</section>
<script src="datetimepicker-master/jquery.js"></script>
<script src="datetimepicker-master/build/jquery.datetimepicker.full.min.js"></script>
<script>
jQuery('#datetimepicker').datetimepicker({
 timepicker:true,
 formatDate:'Y/m/d',
 minDate:'-1970/01/01',
});
</script>
	</body>
</html>