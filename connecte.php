<?php session_start(); ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Rugby-CAR</title>
		<link rel="stylesheet" href="style.css">
		<script src="sweetalert-master/dist/sweetalert.min.js"></script>
		<link rel="stylesheet" type="text/css" href="sweetalert-master/dist/sweetalert.css">
	</head>
	<body>
		<?php include('header.acc.php'); ?>
		<?php if(isset($_SESSION['email'])) {
			include('aside.connecte.php');
		} else { include('aside.acc.php'); }?> 
	<section >
		<?php
		if(isset($message))
        {
              ?><div class="message"> <?php echo $message ;?>  </div><?php
        }
		?>
		<h1>Connection</h1>
			<div id="connexion">
				<form method="post" action="connecte.php">
					<label class="inc" for="mail">Email</label>
					<input class="inc" type="text" id="mail" name="mail" placeholder="xxx@yyy.zzz" required>
					<label class="inc" for="password">Mot de passe</label>
					<input class="inc" type="password" id="password" name="password" placeholder="*******" required><br>
					<input id="btnregister" type="submit" name="submit" value="Login">
					<button class="ins" type="button" name="Annuler" onclick="self.location.href='accueil.php'" value="ok">Annuler</button>
				</form>
			</div>	
	</section>

	</body>
</html>

<?php 
	// connexion à MySQL 
	include_once("includes\param.inc.php"); 

	$mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
	/* Vérification de la connexion 
	if ($mysqli->connect_error) {
	    echo 'connexion impossible... :'.$mysqli->connect_error;
	}
	else {
	    echo 'connexion réussie : '.$mysqli->host_info;
	}
	*/
	if(isset($_POST) && !empty($_POST['mail']) && !empty($_POST['password'])) {
	  	//Hash du mot de passe
		$password = hash("sha256", $_POST['password']);
	 	//extract($_POST);
	 	// on recupére le password de la table qui correspond au login du visiteur
	 	$login = mysqli_escape_string($mysqli, $_POST['mail']);
	 	$sql = "select password,idmembre,prenom,nom from membre where email='".$login."'";
	 	$req = mysqli_query($mysqli, $sql) or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error());
	  	$data = mysqli_fetch_assoc($req);
	  if($data['password'] != $password) {
	  	$message = 'Mauvais login / password merci de recommencer !';
	  	echo '<script>swal({title: "Error!",text: "Mauvais login / password merci de recommencer !",type: "error",confirmButtonText: "OK" })</script>';
	  	
	  }else{
	    $_SESSION['email'] = $login;
	    $_SESSION['id'] = $data['idmembre'];
	    $_SESSION['prenom'] = $data['prenom'];
	    $_SESSION['nom'] = $data['nom'];
	    $message = 'Vous êtes bien connecté en tant que '.$_SESSION['nom'].' '.$_SESSION['prenom'].' !';
	   	echo '<script>swal("Vous êtes bien connecté en tant que '.$_SESSION['nom'].' '.$_SESSION['prenom'].' !", "Vous allez être redirigé vers la page d\'acceuil membre", "success")</script>';
	   	
	    echo '<meta http-equiv="refresh" content="5; URL=bienvenu.php">';
	  }    
	}
 ?>